package maniacalBlocks;

public class Move {
    public static final int DIRECTION_NORTH = 0;
    public static final int DIRECTION_SOUTH = 1;
    public static final int DIRECTION_WEST = 2;
    public static final int DIRECTION_EAST = 3;
    public static final int DIRECTION_STOP = 4;
    public static final int DIRECTION_WIN = 5;
    public static final int DIRECTION_LOSE = 6;

    private final int direction;
    private final Position position;

    public Move(int direction, Position position) {
        this.direction = direction;
        this.position = position;
    }

    public int getDirection() {
        return direction;
    }

    public Position getPosition() {
        return position;
    }
}
