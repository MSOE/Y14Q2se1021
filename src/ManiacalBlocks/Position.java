package maniacalBlocks;

public class Position {
    public final int col;
    public final int row;

    public Position(int col, int row) {
        this.col = col;
        this.row = row;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if(obj instanceof Position) {
            Position pos = (Position)obj;
            equal = pos.col==col && pos.row==row;
        }
        return equal;
    }
}
