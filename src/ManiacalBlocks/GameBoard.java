package maniacalBlocks;

import maniacalBlocks.tiles.Tile;
import maniacalBlocks.tiles.TileFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GameBoard extends JPanel {
    private Tile[][] gameboard;
    private Position playerPos;

    public GameBoard(String filename) throws FileNotFoundException {
        loadBoard(filename);
        createGUI();
    }

    private void loadBoard(String filename) throws FileNotFoundException {
        int height = 0;
        int width;
        String characters = "";
        try (Scanner scan = new Scanner(new FileInputStream(filename))) {
            while(scan.hasNextLine()) {
                characters += scan.nextLine();
                height++;
            }
            width = characters.length()/height;
            gameboard = new Tile[height][width];
            ActionListener moveHandler = new HandleMove();
            for(int row=0; row<height; row++) {
                for(int col=0; col<width; col++) {
                    char type = characters.charAt(row*width + col);
                    gameboard[row][col] = TileFactory.generateTile(type, new Position(col, row));
                    if(type=='@') {
                        playerPos = new Position(col, row);
                        gameboard[row][col].playerVisits();
                    }
                    gameboard[row][col].addActionListener(moveHandler);
                }
            }
        }
    }

    private void enableTiles(boolean enable) {
        for(int row=0; row<numRows(); ++row) {
            for(int col=0; col<numCols(); ++col) {
                gameboard[row][col].setEnabled(enable);
            }
        }
    }

    public int numRows() {
        return gameboard.length;
    }

    public int numCols() {
        return gameboard[0].length;
    }

    private void createGUI() {
        setLayout(new GridLayout(numRows(), numCols()));
        for(int row=0; row<numRows(); ++row) {
            for(int col=0; col<numCols(); ++col) {
                add(gameboard[row][col]);
            }
        }
    }

    private class HandleMove implements ActionListener {
        private static final int TIMER_DELAY = 100;
        private int direction;
        private Timer timer;

        @Override
        public void actionPerformed(ActionEvent e) {
            Tile tile = (Tile)e.getSource();
            Position clickedPos = tile.getPosition();
            if(!clickedPos.equals(playerPos) &&
                    (playerPos.row==clickedPos.row || playerPos.col==clickedPos.col)) {
                setDirection(clickedPos);
                timer = new Timer(TIMER_DELAY, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Move currentMove = new Move(direction, playerPos);
                        try {
                            Tile nextTile = findNextTile(direction, playerPos);
                            Move nextMove = nextTile.processMove(currentMove);
                            if(!nextMove.getPosition().equals(currentMove.getPosition())) {
                                gameboard[playerPos.row][playerPos.col].playerExits();
                                nextTile.playerVisits();
                                direction = nextMove.getDirection();
                                playerPos = nextTile.getPosition();
                            }
                            if(nextMove.getDirection()==Move.DIRECTION_WIN) {
                                JOptionPane.showMessageDialog(GameBoard.this, "Winner");
                                timer.stop();
                            }
                            if(nextMove.getDirection()==Move.DIRECTION_LOSE) {
                                JOptionPane.showMessageDialog(GameBoard.this, "Loser");
                                timer.stop();
                            }
                            if(nextMove.getDirection()==Move.DIRECTION_STOP) {
                                enableTiles(true);
                                timer.stop();
                            }
                        } catch(ArrayIndexOutOfBoundsException ex) {
                            gameboard[playerPos.row][playerPos.col].playerExits();
                            JOptionPane.showMessageDialog(GameBoard.this, "Loser");
                            timer.stop();
                        }
                    }
                });
                enableTiles(false);
                timer.start();
            }
        }

        private void setDirection(Position clickPos) {
            direction = Move.DIRECTION_NORTH;
            if(clickPos.col<playerPos.col) {
                direction = Move.DIRECTION_WEST;
            } else if(clickPos.col>playerPos.col) {
                direction = Move.DIRECTION_EAST;
            } else if(clickPos.row>playerPos.row) {
                direction = Move.DIRECTION_SOUTH;
            }
        }

        private Tile findNextTile(int direction, Position position) {
            int row = position.row;
            int col = position.col;
            switch(direction) {
                case Move.DIRECTION_NORTH:
                    --row;
                    break;
                case Move.DIRECTION_SOUTH:
                    ++row;
                    break;
                case Move.DIRECTION_WEST:
                    --col;
                    break;
                case Move.DIRECTION_EAST:
                    ++col;
                    break;
            }
            return gameboard[row][col];
        }
    }
}








