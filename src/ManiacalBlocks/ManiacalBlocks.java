package maniacalBlocks;

import javax.swing.*;
import java.io.FileNotFoundException;

public class ManiacalBlocks extends JFrame {

    private static final int WIDTH = 500;
    private static final int HEIGHT = 300;

    public static void main(String[] args) {
        new ManiacalBlocks();
    }

    public ManiacalBlocks() {
        super("maniacalBlocks");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        GameBoard gameBoard = null;
        try {
            gameBoard = new GameBoard("levels/level.txt");
        } catch (FileNotFoundException e) {
            JFileChooser chooser = new JFileChooser("levels");
            int status = chooser.showOpenDialog(this);
            if(status==JFileChooser.APPROVE_OPTION) {
                try {
                    gameBoard = new GameBoard(chooser.getSelectedFile().getAbsolutePath());
                } catch (Exception e1) {
                    System.err.println("File not found.");
                }
            } else {
                System.out.println("Fine, play a different game then.");
                System.exit(-1);
            }
        }
        add(gameBoard);
        setVisible(true);
    }

}
