package maniacalBlocks.tiles;

import maniacalBlocks.Move;
import maniacalBlocks.Position;

public class SETile extends Tile {

	public SETile(Position position) {
		super(position, 'r');
	}

	@Override
	public Move processMove(Move move) {
		Move nextMove = null;

		// Change directions as normal
		if(move.getDirection() == Move.DIRECTION_WEST) {
			nextMove = new Move(Move.DIRECTION_SOUTH, position);
		} else if(move.getDirection() == Move.DIRECTION_NORTH) {
			nextMove = new Move(Move.DIRECTION_EAST, position);
		} else {
			nextMove = new Move(Move.DIRECTION_STOP, move.getPosition());
		}

		return nextMove;
	}
}
