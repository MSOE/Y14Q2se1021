package maniacalBlocks.tiles;

import maniacalBlocks.Move;
import maniacalBlocks.Position;

public class TunnelNS extends Tile {
    public TunnelNS(Position position) {
        super(position, 'H');
    }

    @Override
    public Move processMove(Move move) {
        if(move.getDirection() == Move.DIRECTION_EAST || move.getDirection() == Move.DIRECTION_WEST) {
            return new Move(Move.DIRECTION_STOP, move.getPosition());
        } else {
            return new Move(move.getDirection(), this.getPosition());
        }
    }
}