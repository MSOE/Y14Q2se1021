package maniacalBlocks.tiles;

import maniacalBlocks.Move;
import maniacalBlocks.Position;

public class NETile extends Tile {

	public NETile(Position position) {
		super(position, 'L');
	}

	@Override
	public Move processMove(Move move) {
		Move nextMove = null;

		// Change directions as normal
		if(move.getDirection() == Move.DIRECTION_WEST) {
            nextMove = new Move(Move.DIRECTION_NORTH, position);
		} else if(move.getDirection() == Move.DIRECTION_SOUTH) {
            nextMove = new Move(Move.DIRECTION_EAST, position);
		} else {
			nextMove = new Move(Move.DIRECTION_STOP, move.getPosition());
		}

		return nextMove;
	}
}
