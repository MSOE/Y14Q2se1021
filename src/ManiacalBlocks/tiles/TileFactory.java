package maniacalBlocks.tiles;

import maniacalBlocks.Position;

public class TileFactory {
    public static Tile generateTile(char type, Position position) {
        Tile tile = null;
        switch (type) {
            case 'X':
                tile = new FinishTile(position);
                break;
            case '=':
                tile = new HorizontalTunnel(position);
                break;
            case 'H':
                tile = new TunnelNS(position);
                break;
            case 'r':
                tile = new SETile(position);
                break;
            case 'L':
                tile = new NETile(position);
                break;
            case 'J':
                tile = new NWTile(position);
                break;
            case '@':
            default:
                tile = new Tile(position);
        }
        return tile;
    }
}
