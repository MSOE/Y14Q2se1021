package maniacalBlocks.tiles;


import maniacalBlocks.Move;
import maniacalBlocks.Position;

public class HorizontalTunnel extends Tile
{

    /**
     * Creates a new HorizontalTunnel Tile with the given position and '=' as its type.
     * @param position The Position of this Tile.
     */
    public HorizontalTunnel(Position position) {
        super(position, '=');
    }

    /**
     * Returns a new Move object that instructs the player to stop if approaching this tile from the north or the south.
     * @param move The Move that the player is currently attempting to make.
     * @return  A Move with a direction set to DIRECTION_STOP and the player's position if the player is approaching
     *             from the north or south, or with a direction that preserves the player's direction and this tile's position.
     */
    @Override
    public Move processMove(Move move) {
        Move nextMove = null;
        if(move.getDirection() == Move.DIRECTION_NORTH || move.getDirection() == Move.DIRECTION_SOUTH) {
            nextMove = new Move(Move.DIRECTION_STOP, move.getPosition()); }
        else if(move.getDirection() == Move.DIRECTION_EAST || move.getDirection() == Move.DIRECTION_WEST) {
            nextMove = new Move(move.getDirection(), position);
        }

        return nextMove;
    }
}
