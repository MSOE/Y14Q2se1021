package maniacalBlocks.tiles;

/*
L - enter from N/E Matt
7 - enter from W/S Tyler
J - enter from N/W
r - enter from S/E
H - enter from N/S Nick
= - enter from W/E Ryan F
# - enter never Aaron
* - star, collect them all for fame and fortune Andrew
d - death (don't really die, just lose) Alex
% - teleport Dylan
 */
import maniacalBlocks.Move;
import maniacalBlocks.Position;

import javax.swing.*;

/**
 * Base tile class for the maniacal blocks gameboard.
 * The base tile allows free movement of a player through
 * the tile without changing the direction of movement.
 * @author taylor and class
 * @version 2014.02.15
 */
public class Tile extends JButton {
    /**
     * Position of the file on the gameboard.
     */
    protected final Position position;

    /**
     * The character representation of the tile type.
     */
    private final char type;

    /**
     * Constructor
     * @param position The location of the tile being created.
     */
    public Tile(Position position) {
        this(position, ' ');
    }

    /**
     * Constructor
     * @param position The location of the tile being created.
     * @param type The character representing the type of tile.
     */
    protected Tile(Position position, char type) {
        super("" + type);
        this.position = position;
        this.type = type;
    }

    /**
     * Determines the next move for the player who is passing through
     * this tile
     * @param move The current move including direction and position
     * @return The next move that the player should make
     */
    public Move processMove(Move move) {
        playerVisits();
        return new Move(move.getDirection(), position);
    }

    /**
     * Indicate that the player is currently visiting this tile.
     */
    public void playerVisits() {
        setText("@");
    }

    /**
     * Indicate that the player is no longer visiting this tile.
     */
	public void playerExits(){
        setText("" + type);
	}

    /**
     * Returns the location of the tile.
     * @return The location of the tile on the gameboard.
     */
    public Position getPosition() {
        return position;
    }
}
