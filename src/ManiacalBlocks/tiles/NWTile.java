package maniacalBlocks.tiles;

import maniacalBlocks.Move;
import maniacalBlocks.Position;

public class NWTile extends Tile {

	public NWTile(Position position) {
		super(position, 'J');
	}

	@Override
	public Move processMove(Move move) {
		Move nextMove = null;

		// Change directions as normal
		if(move.getDirection() == Move.DIRECTION_EAST) {
            nextMove = new Move(Move.DIRECTION_NORTH, position);
		} else if(move.getDirection() == Move.DIRECTION_SOUTH) {
            nextMove = new Move(Move.DIRECTION_WEST, position);
		} else {
			nextMove = new Move(Move.DIRECTION_STOP, move.getPosition());
		}

		return nextMove;
	}
}
