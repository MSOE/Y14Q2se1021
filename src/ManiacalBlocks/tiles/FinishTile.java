package maniacalBlocks.tiles;

import maniacalBlocks.Move;
import maniacalBlocks.Position;

public class FinishTile extends Tile {
    public FinishTile(Position position) {
        super(position, 'X');
    }

    @Override
    public Move processMove(Move move) {
        return new Move(Move.DIRECTION_WIN, position);
    }
}
