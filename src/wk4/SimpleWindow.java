package wk4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleWindow extends JFrame {

    private final static int WIDTH = 450;
    private final static int HEIGHT = 300;
    private JLabel pony;
    private JTextField field;

    public static void main(String[] args) {
        new SimpleWindow();
    }

    public SimpleWindow() {
        super("Dog and Pony Show");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(100, 200);
        setLayout(new FlowLayout());
        pony = new JLabel("");
        JButton show = new JButton("Show");
        JButton hide = new JButton("Hide");
        field = new JTextField(10);
        field.addActionListener(new TextFieldListener());
        add(field);
        show.addActionListener(new ShowButtonListener(this));
        hide.addActionListener(new HideButtonListener());
        add(new JLabel("Dog"));
        add(pony);
        add(show);
        add(hide);
        setVisible(true);
    }

    public void showAction() {
        pony.setText(pony.getText() + " pony");
    }

    private class HideButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(pony.getText().length()>4) {
                pony.setText(pony.getText().substring(5));
            }
        }
    }

    private class TextFieldListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            setTitle(field.getText());
        }
    }
}
