package wk4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class BinaryCalculator extends JFrame implements WindowListener {

    private final int WIDTH = 170;
    private final int HEIGHT = 300;
    private JTextField display;
    private String addend1 = "";

    public static void main(String[] ignored) {
        new BinaryCalculator();
    }

    public BinaryCalculator() {
        super("Calculator");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(100, 200);
        setLayout(new BorderLayout());
        Container x = getContentPane();
        display = new JTextField(12);
        display.setEditable(false);
        x.add(display, BorderLayout.NORTH);
        x.add(createButtonPanel());
        JButton clear = new JButton("Clear");
        add(clear, BorderLayout.SOUTH);
        clear.addActionListener(new HandleClearButton());
        addWindowListener(this);

        setVisible(true);
    }

    private JPanel createButtonPanel() {
        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(2,2, 8, 8));
        addNumbers(buttons);
        JButton plus = new JButton("+");
        JButton equals = new JButton("=");
        buttons.add(plus);
        buttons.add(equals);
        plus.addActionListener(new HandlePlusButton());
        equals.addActionListener(new HandleEqualsButton());
        return buttons;
    }

    private void addNumbers(JPanel buttons) {
        for(char digit='0'; digit<='1'; ++digit) {
            JButton number = new JButton("" + digit);
            number.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    display.setText(display.getText() + e.getActionCommand());
                }
            });
            buttons.add(number);
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("Window opened");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.err.println("Window closing");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.err.println("Window closed");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        System.out.println("Window iconified");
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // Do nothing
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // Do nothing
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // Do nothing
    }

    private class HandleClearButton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            display.setText("");
        }
    }

    private class HandlePlusButton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            addend1 = display.getText();
            display.setText("");
        }
    }

    private class HandleEqualsButton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String addend2 = display.getText();
            if(addend1.equals("") || addend2.equals("")) {
                display.setText("ERROR");
                display.setForeground(Color.RED);
            } else {
                display.setForeground(Color.BLACK);
                int result = Integer.parseInt(addend1, 2) +
                         Integer.parseInt(addend2, 2);
                addend1 = Integer.toBinaryString(result);
                display.setText(addend1);
            }
        }
    }
}
