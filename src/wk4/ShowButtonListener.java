package wk4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowButtonListener implements ActionListener {
    private SimpleWindow gui;

    public ShowButtonListener(SimpleWindow gui) {
        this.gui = gui;
    }

    public void actionPerformed(ActionEvent e) {
        gui.showAction();
    }
}
