package wk6;

public class ClassBoredException extends RuntimeException {
    public ClassBoredException(String message) {
        super(message);
    }
}
