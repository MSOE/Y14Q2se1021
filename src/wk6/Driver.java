package wk6;

import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 Try block indicates it's looking to handle an exception
  if it occurs.
 Once exception occurs, the rest of the try block is ignored
 If a matching catch block is present, it runs and then program
  continues after all catch blocks
 If no match (or no try block present), then returns immediately
  from method and looks for try block in calling method
 */
public class Driver {
    public static void main(String[] ignored) {
        try {
            System.out.println("one");
            main3();
            System.out.println("two");
        } finally {
            System.out.println("three");
        }
    }

    public static void main3() throws ClassBoredException {
        String input = JOptionPane.showInputDialog(null, "How old are you (in years)");
        int age;
        try {
            if(true) {
                throw new ClassBoredException("This isn't really an arithemtic exception.");
            }
            age = Integer.parseInt(input);
            JOptionPane.showMessageDialog(null, "I'm guessing you'll be " + (age+1) + " next year");
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error!  You should enter an integer value");
        }

        JOptionPane.showMessageDialog(null, "No worries.  Either way we'll continue on with our program");
    }







    public static void main2(String[] ignored) {
        Scanner in = new Scanner(System.in);
        boolean inputValid = false;
        int age;
        while(!inputValid) {
            try {
                age = in.nextInt();
                inputValid = true;
                System.out.println("The square root of your age is: " + Math.sqrt(age));
            } catch(InputMismatchException e) {
                in.next();
                System.out.println("You entered invalid input");
            }
        }
    }
}
