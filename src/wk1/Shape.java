package wk1;

import java.awt.Color;

public abstract class Shape implements Displayable {
    private Color color;
    private double xCoord;
    private double yCoord;

    public Shape() {
        this(0.0, 0.0);
        System.out.println("Shape: constructor");
    }

    public Shape(double x, double y) {
        this(x, y, Color.WHITE);
        System.out.println("Shape: 2-arg constructor");
    }

    public Shape(double x, double y, Color color) {
        xCoord = x;
        yCoord = y;
        this.color = color;
        System.out.println("Shape: 3-arg constructor");
    }

    public void display() {
        System.out.println("Shape: display");
        draw();
    }

    public abstract void draw();

    public abstract void erase();

    public void move() {
        xCoord += 5.0;
        yCoord += 5.0;
        System.out.println("Shape: move");
    }

    public abstract void zoom(double magnification);

    public Color getColor() {
        System.out.println("Shape: getColor");
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        System.out.println("Shape: setColor");
    }

    @Override
    public String toString() {
        return super.toString() + ": Shape at (" + xCoord + ", " + yCoord + ")";
    }
}
