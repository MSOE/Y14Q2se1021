package wk1;

import java.util.ArrayList;
import java.util.List;

public class Driver {
    public static void main(String[] ignored) {
        Displayable displayable = new Dog();
        List<Shape> showOffs = new ArrayList<>();
        showOffs.add(new Triangle());
        showOffs.add(new Point());
        showOffs.add(new Point());
        for(Shape shape : showOffs) {
            shape.draw();
        }
    }
}







/*
Composition: A class is said to be composed of additional objects
             if it has attributes that are completely controlled
             by the class.

Aggregation: A class aggregates other objects when it contains
             references to other objects but does not control
             the creation and destruction of the objects to
             which the references point.
 */
