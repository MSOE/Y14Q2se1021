/*
 * Course: SE1021-011
 * Term: Winter 2013-2014
 * Assignment: Quiz 1
 * Author: Chris Taylor
 * Date: 2013-12-5
 */
package wk1;

import java.util.ArrayList;

/**
 * Program that demonstrates the solution to part b of Quiz 1.
 * The program creates an ArrayList of strings and calls the
 * method, getLongest(), required by the quiz question.
 * @author taylor@msoe.edu
 * @version 20131205.1
 */
public class Quiz1 {

    /**
     * A useless attribute that should not be here
     */
    boolean useless = true;

    /**
     * Main program that creates a hardcoded array list, calls the
     * required method and displays the result.
     * @param ignored Ignored
     */
    public static void main(String[] ignored) {
        ArrayList<String> words = new ArrayList<>();
        words.add("The");
        words.add("quiz");
        words.add("was");
        words.add("easy");
        words.add("to");
        words.add("do.");
        System.out.println(getLongest(words));
    }

    /**
     * Returns the longest string in the list of words passed to the method
     * @param words An arraylist of strings that will be used to find the longest
     * @return The longest string in words
     */
    public static String getLongest(ArrayList<String> words) {
        String longest = "";
        if(words!=null) {
            for(String word : words) {
                if(word.length()>longest.length()) {
                    longest = word;
                }
            }
// Alternative for loop A
//            for(int i=0; i<words.size(); ++i) {
//                String word = words.get(i);
//                if(word.length()>longest.length()) {
//                   longest = word;
//                }
//            }
// Alternative for loop B
//            for(int i=0; i<words.size(); ++i) {
//                if(words.get(i).length()>longest.length()) {
//                    longest = words.get(i);
//                }
//            }
// Alternative for loop C (this one doesn't require the if statement around it)
//            for(int i=0; words!=null && i<words.size(); ++i) {
//                if(words.get(i).length()>longest.length()) {
//                    longest = words.get(i);
//                }
//            }
        }
        return longest;
    }
















}
