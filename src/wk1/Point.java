package wk1;

public class Point extends Shape {

    public void draw() {
        System.out.println("Point: draw");
    }

    @Override
    public void erase() {
        System.out.println("Point: erase");
    }

    @Override
    public void zoom(double magnification) {
        System.out.println("Point: zoom");
    }
}
