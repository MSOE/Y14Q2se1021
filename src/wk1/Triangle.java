package wk1;

public class Triangle extends Shape {
    private double xCoord2;
    private double yCoord2;
    private double xCoord3;
    private double yCoord3;

    public Triangle() {
        super(0.0, 0.0);
        xCoord2 = 1.0;
        yCoord2 = 0.0;
        xCoord3 = 1.0;
        yCoord3 = 1.0;
        System.out.println("Triangle: Constructor");
    }

    public void draw() {
        System.out.println("Triangle: draw");
    }

    @Override
    public void erase() {
        System.out.println("Triangle: erase");
    }

    @Override
    public void zoom(double magnification) {
        System.out.println("Triangle: zoom");
    }
}
