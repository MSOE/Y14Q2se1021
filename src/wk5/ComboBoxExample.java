package wk5;
/**************************************************************
* ComboBoxExample.java
* Dean & Dean
*
* This class implements a combo box for days of the week.
**************************************************************/

import javax.swing.*;
import java.awt.*;

public class ComboBoxExample extends JFrame
{
  public ComboBoxExample()
  {
    setTitle("Combo Box Example");
    setSize(300, 100);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    createContents();
    setVisible(true);
  } // end ComboBoxExample constructor

  //**************************************

  // Create components and add to window.

  private void createContents()
  {
    final String[] DAYS =
      {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
    JComboBox daysBox;
    
    setLayout(new FlowLayout());
    add(new JLabel("Select a day:"));
    daysBox = new JComboBox(DAYS);
    add(daysBox);
  } // end createContents

  //**************************************

  public static void main(String[] args)
  {
    new ComboBoxExample();
  }
} // end class ComboBoxExample
