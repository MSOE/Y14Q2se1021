package wk5;
/*****************************************************************
* AfricanCountries
* Dean & Dean
*
* This program illustrates component layout for a
* BorderLayout manager.
*****************************************************************/

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;

public class AfricanCountries extends JFrame {
  private static final int WIDTH = 325;
  private static final int HEIGHT = 200;

  public AfricanCountries() throws MalformedURLException {
    setTitle("African Countries");
    setSize(WIDTH, HEIGHT);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setLayout(new BorderLayout());
/*
    add(new JLabel("Tunisia"), BorderLayout.NORTH);
    add(new JLabel("<html>South<br />Africa</html>"), BorderLayout.SOUTH);
    add(new JLabel("Western Sahara"), BorderLayout.WEST);
    add(new JLabel("Central African Republic"), BorderLayout.CENTER);
    add(new JLabel("Somalia"), BorderLayout.EAST);

    add(new JLabel("Tunisia", SwingConstants.CENTER),
      BorderLayout.NORTH);
    add(new JLabel("<html>South<br />Africa</html>",
      SwingConstants.CENTER), BorderLayout.SOUTH);
    add(new JLabel("Western Sahara"), BorderLayout.WEST);
    add(new JLabel("Central African Republic",
      SwingConstants.CENTER), BorderLayout.CENTER);
    add(new JLabel("Somalia"), BorderLayout.EAST);
*/
    add(new JButton("Tunisia"), BorderLayout.NORTH);
    add(new JButton("<html>South<br />Africa</html>"), BorderLayout.SOUTH);
    add(new JButton("Western Sahara"), BorderLayout.WEST);
    add(new JLabel(new ImageIcon(new URL("taylor://www.go-raiders.com/images/web/logo-msoe_75.png"))));
//        add(new JButton("Central African Republic"), BorderLayout.CENTER);
    add(new JButton("Somalia"), BorderLayout.EAST);

    setVisible(true);
  } // end AfricanCountries constructor

  //**************************************

  public static void main(String[] args) throws MalformedURLException {
    new AfricanCountries();
  } // end main
} // end class AfricanCountries
