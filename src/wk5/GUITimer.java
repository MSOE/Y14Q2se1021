/*
 * Based on code from Dr. Sebern.
 */
package wk5;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUITimer extends JFrame {

    /*
    An inner class (anonymous or normal) is preferred when
    it needs to access private members of the outer class.
    An anonymous inner class may be preferred if only one
    instance of the inner class is required and the implementation
    of the inner class is brief.  This allows the functionality
    associated with the implementation to be located near the
    button connection (in the case of an ActionListener).
     */
    private static final int WINDOW_WIDTH = 200;
    private static final int WINDOW_HEIGHT = 200;
    private static int TIMER_ONE_SECOND_DELAY = 1000;

    private JTextField addend1Field;
    private JTextField addend2Field;
    private JTextField answerField;
    private JTextField elapsedTimeField;
    private int elapsedTime = 0;

    private Color defaultBackgroundColor;

    private javax.swing.Timer timer;

    public static void main(String[] args) {
        GUITimer gui = new GUITimer();
        gui.setVisible(true);
        System.out.println("Finished GUI initialization.");
    }

    public GUITimer() {
        super("GUI Timer");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setLayout(new FlowLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        add(new JLabel("Enter x: "));
        addend1Field = new JTextField(10);
        add(addend1Field);
        add(new JLabel("Enter y: "));
        addend2Field = new JTextField(10);
        add(addend2Field);

        add(new JLabel("Answer: x+y = "));
        answerField = new JTextField(10);
        answerField.setEditable(false);
        add(answerField);

        defaultBackgroundColor = answerField.getBackground();

        JButton calcButton = new JButton("Calculate");
        calcButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                calculate();
            }
        });
        add(calcButton);

        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addend1Field.setText("");
                addend2Field.setText("");
                answerField.setText("");
                answerField.setBackground(defaultBackgroundColor);
                elapsedTime = 0;
                displayTime();
                timer.start();
            }
        });
        add(resetButton);

        add(new JLabel("Elapsed Time"));
        elapsedTimeField = new JTextField(5);
        elapsedTimeField.setEditable(false);
        add(elapsedTimeField);

        timer = new javax.swing.Timer(TIMER_ONE_SECOND_DELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elapsedTime++;
                displayTime();
            }
        });
        timer.start();
    }

    private void calculate() {
        String input1String = addend1Field.getText();
        String input2String = addend2Field.getText();
        String answerString = "???";
        Color answerColor = Color.red;

        if ((!input1String.isEmpty()) &&
                (!input2String.isEmpty())) {
            double input1 = Double.parseDouble(input1String);
            double input2 = Double.parseDouble(input2String);
            double answer = input1 + input2;
            answerString = Double.toString(answer);
            answerColor = Color.green;
        }

        answerField.setText(answerString);
        answerField.setBackground(answerColor);
        timer.stop();
        displayTime();
    }

    private void displayTime() {
        elapsedTimeField.setText("" + elapsedTime);
    }
}
