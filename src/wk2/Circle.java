/**
 * Author: Josiah Yoder et al.
 * Class: SE1011-011
 * Date: 12/6/13 11:35 AM
 * Lesson: Week 1, Day 3
 *
 */
package wk2;

/**
 * A Circle class
 * @author Josiah Yoder
 * @version 2013-12-6
 */
public class Circle extends Shape implements Displayable {


    private double radius;

    /**
     * Construct a new Circle.
     *
     * @param centerX the x (horizontal) component of the center position
     *                measured in pixels
     * @param centerY the y (vertical) component
     *                measured in pixels
     * @param radius the radius of the circle
     *                measured in pixels
     *
     * Please note: This example does not compile because
     *              the Circle class can't make a default shape
     *              (there is no default Shape constructor)
     */
    public Circle(double centerX, double centerY,
        double radius) {
        super(centerX, centerY);
        this.radius = radius;
        /* Potential Solutions:
         * 1. Make a default constructor
         * 2.a. (doesn't work) this keyword
         * 2.b. (works) Use super keyword to call constructor from previous class
        */
        // Alternative set approaches:
//        this.setCenter(centerX,centerY);
//        super.setCenter(centerX,centerY);
//        setCenter(centerX,centerY);
    }

    /**
     * Prints "Circle with radius XX.X and center (XX.X, XX.X)"
     * @return String representation
     */
    public String toString() {
        // TODO:
        // 1. Implement this method (except for printing center)
        // 2. How to print the private center variables?
        // return "("+super.centerX+", "+" ...";
        return "Circle with radius " + radius + " and " + super.toString();

    }

}
