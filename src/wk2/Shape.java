package wk2;

/**
 * Author: Josiah Yoder et al.
 * Class: SE1011-011
 * Date: 12/6/13 11:32 AM
 * Lesson: Week 7, Day 1
 */
public class Shape implements Displayable {
    private double centerX;
    private double centerY;
    private String name;

    public Shape () {
        this(0,0);
    }

    public void display() {
        System.out.println(toString());
    }

    public Shape (double centerX, double centerY) {
        this(centerX, centerY, "");
    }

    public Shape (double centerX, double centerY, String name) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.name = name;
    }

    public void setCenter(double x, double y) {
        centerX = x;
        centerY = y;
    }

    public String toString() {
        return "center at ("+centerX+", "+centerY+")";
    }
}
