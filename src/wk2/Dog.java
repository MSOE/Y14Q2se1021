package wk2;

public class Dog implements Displayable {

    private String name = "Spotless";

    @Override
    public void display() {
        System.out.println(toString() + "I am a dog named " + name);
    }
}
