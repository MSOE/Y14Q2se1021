/**
 * Author: Josiah Yoder et al.
 * Class: SE1011-011
 * Date: 12/10/13 11:13 AM
 * Lesson: Week 7, Day 1
 */
package wk2;

import java.util.ArrayList;
import java.util.List;

public class DrawableDrive {

    public static void main(String[] args) {
        List<Displayable> displayables = new ArrayList<>();
        displayables.add(new Circle(0.0, 3.0, 3));
        displayables.add(new Dog());
        displayables.add(new Shape());
        display(displayables);
    }

    public static void display(List<Displayable> displayables) {
        for(Displayable s : displayables) {
            s.display();
        }

    }
}







