package wk8;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Driver {

    public static void main(String[] ignored) {
        Data a = new Data(Math.PI, 2, "Ryker");
        Data b = new Data(Math.E, -2, "Andrew");
        write("data.dat", false, a, b);
        read("data.dat");
    }

    public static void read(String filename) {
        try (ObjectInputStream in = new ObjectInputStream(
                new FileInputStream(filename))) {
            while(true) {
                System.out.println(in.readObject());
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        } catch (EOFException e) {
            System.out.println("done reading file");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
    public static void write(String filename, boolean append, Object a, Object b) {
        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream(filename, append))) {
            out.writeObject(a);
            out.writeObject(b);
            out.writeObject(a);
            out.writeObject(b);
            out.writeObject(b);
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void displayDirectory(String dirName) {
        Path path = Paths.get(dirName);
        try (DirectoryStream<Path> directory =
                     Files.newDirectoryStream(path)) {
            for(Path p : directory) {
                System.out.println(p.getFileName());
            }
        } catch (IOException e) {
            System.err.println("IO exception occurred");
        }
    }

}
