package wk8;

import java.io.Serializable;

public class Data implements Serializable {
    private double aa;
    private int i;
    private String name;

    public Data(double a, int b, String c) {
        aa = a;
        i = b;
        name = c;
    }

    @Override
    public String toString() {
        return name + " " + i + " " + aa;
    }
}
